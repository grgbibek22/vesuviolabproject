﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VesuvioLabsProject.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Age { get; set; }
        public string GroupName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}