﻿using System.Data.Entity;
using VesuvioLabsProject.Models;

namespace VesuvioLabsProject
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("dbConnectionString")
        {
            Database.SetInitializer<ApplicationContext>(new CreateDatabaseIfNotExists<ApplicationContext>());
        }
        public DbSet<User> Users { get; set; }
    }
}