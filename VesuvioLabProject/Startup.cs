﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(VesuvioLabsProject.Startup))]

namespace VesuvioLabsProject
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
