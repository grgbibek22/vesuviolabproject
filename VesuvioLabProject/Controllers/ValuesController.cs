﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VesuvioLabsProject.Auth;
using VesuvioLabsProject.Models;

namespace VesuvioLabsProject.Controllers
{
    public class ValuesController : ApiController
    {

        // GET api/values
        [BasicAuthentication]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [BasicAuthentication]
        public IHttpActionResult Post([FromBody] User model)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid Data");
            }
            using (var dbContext = new ApplicationContext())
            {
                //var groupId = dbContext.Groups.FirstOrDefault(x => x.GroupName == model.GroupName)?.Id;
                //if (groupId == null || groupId < 1)
                //    throw new ArgumentException("The groupname doesn't exist");
                var user = new User()
                {
                    FullName = model.FullName,
                    Age = model.Age,
                    GroupName = model.GroupName,
                    CreatedDate = DateTime.Now
                };
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
            }
            if(model.GroupName.ToLower() == "avenger")
                UserHub.NotifyCurrentEmployeeInformationToAllClients();

            return Ok();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }


    }
}
