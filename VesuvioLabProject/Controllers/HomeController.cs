﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using VesuvioLabsProject.Models;

namespace VesuvioLabsProject.Controllers
{
    public class HomeController : Controller
    {
        List<User> userList;

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetAllUserRecords()
        {
            using (var context = new ApplicationContext())
            {
                userList = context
                .Users
                .ToList();
            }          
            return PartialView("_UserList", userList);
        }


        public ActionResult Insert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert(User user)
        {  
                if (ModelState.IsValid)
                {
                    using (var context = new ApplicationContext())
                    { 
                            context.Users.Add(user);
                            context.SaveChanges();                       
                    }                                       
                }
           
            UserHub.NotifyCurrentEmployeeInformationToAllClients();
            return RedirectToAction("Index");
        }

    }
}