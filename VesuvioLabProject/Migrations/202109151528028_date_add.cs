﻿namespace VesuvioLabsProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class date_add : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "CreatedDate");
        }
    }
}
